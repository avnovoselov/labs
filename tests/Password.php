<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class Password extends TestCase
{
	public function testCheck()
	{
		require_once __DIR__ . "/../src/Snippet/Password.php";

		// COMMON
		$this->assertFalse(
			\Snippet\Password::check('', \Snippet\Password::CHECK_ANY),
			'empty password'
		);
		$this->assertTrue(
			\Snippet\Password::check('foobar', \Snippet\Password::CHECK_ANY, 3, 6),
			'any password [3-6]'
		);
		$this->assertFalse(
			\Snippet\Password::check('foo', \Snippet\Password::CHECK_ANY, 4, 8),
			'wrong any password [4-8]'
		);
		$this->assertFalse(
			\Snippet\Password::check('foobar', \Snippet\Password::CHECK_ANY, 3, 5),
			'wrong any password length [3-5]'
		);
		$this->assertTrue(
			\Snippet\Password::check('<Fo0__322__BaR>', \Snippet\Password::CHECK_ANY),
			'any password'
		);
		$this->assertTrue(
			\Snippet\Password::check('FOObAR', \Snippet\Password::CHECK_LOWER_CASE),
			'lower case password'
		);
		$this->assertFalse(
			\Snippet\Password::check('FOOBAR', \Snippet\Password::CHECK_LOWER_CASE),
			'wrong lower case password'
		);
		$this->assertTrue(
			\Snippet\Password::check('fooBar', \Snippet\Password::CHECK_UPPER_CASE),
			'upper case password'
		);
		$this->assertFalse(
			\Snippet\Password::check('foobar', \Snippet\Password::CHECK_UPPER_CASE),
			'wrong upper case password'
		);
		$this->assertTrue(
			\Snippet\Password::check('foobar322', \Snippet\Password::CHECK_DIGIT),
			'digit password'
		);
		$this->assertFalse(
			\Snippet\Password::check('foobar', \Snippet\Password::CHECK_DIGIT),
			'wrong digit password'
		);
		$this->assertTrue(
			\Snippet\Password::check('_foobar', \Snippet\Password::CHECK_SPEC),
			'spec password'
		);
		$this->assertFalse(
			\Snippet\Password::check('foobar', \Snippet\Password::CHECK_SPEC),
			'wrong spec password'
		);
		$this->assertTrue(
			\Snippet\Password::check(' oobar', \Snippet\Password::CHECK_SPACE),
			'space password'
		);
		$this->assertFalse(
			\Snippet\Password::check('foobar', \Snippet\Password::CHECK_SPACE),
			'wrong space password'
		);

		// VALID combine password
		$this->assertTrue(
			\Snippet\Password::check('foobar322', \Snippet\Password::CHECK_EASY),
			'easy password'
		);
		$this->assertTrue(
			\Snippet\Password::check('fooBar322', \Snippet\Password::CHECK_MEDIUM),
			'medium password'
		);
		$this->assertTrue(
			\Snippet\Password::check('fooBar_322!', \Snippet\Password::CHECK_STRONG),
			'strong password'
		);
		$this->assertTrue(
			\Snippet\Password::check(' fooBar_322!', \Snippet\Password::CHECK_IMPOSSIBLE),
			'impossible password'
		);

		// INVALID combine password
		$this->assertFalse(
			\Snippet\Password::check('foobar', \Snippet\Password::CHECK_EASY),
			'easy password'
		);
		$this->assertFalse(
			\Snippet\Password::check('fooar322', \Snippet\Password::CHECK_MEDIUM),
			'medium password'
		);
		$this->assertFalse(
			\Snippet\Password::check('fooBar322', \Snippet\Password::CHECK_STRONG),
			'strong password'
		);
		$this->assertFalse(
			\Snippet\Password::check('fooBar_322!', \Snippet\Password::CHECK_IMPOSSIBLE),
			'impossible password'
		);
	}

	public function testHash()
	{
		require_once __DIR__ . "/../src/Snippet/Password.php";

		$this->assertContains(
			'dd79d7383ae053279bbd6481696c1d025543ad308e7f1567fa879625d851397e',
			\Snippet\Password::hash('foobar', '1', '**s<dsOsP?'),
			'right hash'
		);
		$this->assertNotContains(
			'dd79d7383ae053279bbd6481696c1d025543ad308e7f1567fa879625d851397e',
			\Snippet\Password::hash('foobar2', '32', '**s<dsOsP?'),
			'wrong hash'
		);

		\Snippet\Password::$HASH_PASSWORD_SALT = 'FOO_BAR_SALT';
		$this->assertContains(
			'0584638df6d08f39da3d953e2ae7fef00a00af0d09de74b4394fcf9e9275dad8',
			\Snippet\Password::hash('foobar', '1'),
			'right hash common salt'
		);

		putenv(\Snippet\Password::$HASH_PASSWORD_SALT_ENVIRONMENT_KEY . "=FOO_BAR_ENV_SALT");
		$this->assertContains(
			'ca7aaf93ca27eb0c9d1dd9dd40d93d3b11d793a691c942e7b8c287761c3a8074',
			\Snippet\Password::hash('foobar', '1'),
			'right hash environment salt'
		);
		putenv(\Snippet\Password::$HASH_PASSWORD_SALT_ENVIRONMENT_KEY);
	}

	public function testComplexity()
	{
		require_once __DIR__ . "/../src/Snippet/Password.php";

		$this->assertEquals(
			\Snippet\Password::COMPLEXITY_IMPOSSIBLE,
			\Snippet\Password::complexity(' X@d4!9AdBeeZ1987. '),
			'impossible password'
		);
		$this->assertEquals(
			\Snippet\Password::COMPLEXITY_STRONG,
			\Snippet\Password::complexity('Bazb@R322'),
			'strong password'
		);
		$this->assertEquals(
			\Snippet\Password::COMPLEXITY_MEDIUM,
			\Snippet\Password::complexity('PhPE0sQ1E'),
			'medium password'
		);
		$this->assertEquals(
			\Snippet\Password::COMPLEXITY_LOW,
			\Snippet\Password::complexity('iWarCraft'),
			'low safety password'
		);
		$this->assertEquals(
			\Snippet\Password::COMPLEXITY_NONE,
			\Snippet\Password::complexity('warcraft'),
			'none safety password'
		);

		$this->assertEquals(
			\Snippet\Password::COMPLEXITY_NONE,
			\Snippet\Password::complexity('passw0rd'),
			'most popular password'
		);
	}
}