<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class LicensePlate extends TestCase
{
	public function testCheck()
	{
		require_once __DIR__ . "/../src/Snippet/LicensePlate.php";

		$this->assertContains('private', \Snippet\LicensePlate::check('C091РС777'));
		$this->assertContains('legal', \Snippet\LicensePlate::check('H452TE777'));
		$this->assertContains('transit', \Snippet\LicensePlate::check('T777XУ199'));

		$this->assertContains('bus', \Snippet\LicensePlate::check('TО777 199'));
		$this->assertContains('taxi', \Snippet\LicensePlate::check('УР423 43'));

		$this->assertContains('trailer', \Snippet\LicensePlate::check('УР1423 178'));

		$this->assertContains('motorcycle', \Snippet\LicensePlate::check('8888АР26'));
		$this->assertContains('motorbike', \Snippet\LicensePlate::check('7341АР01'));
		$this->assertContains('tractor', \Snippet\LicensePlate::check('1988КР95'));
		$this->assertContains('military', \Snippet\LicensePlate::check('1234ВХ67'));

		$this->assertContains('police', \Snippet\LicensePlate::check('М432102'));
		$this->assertContains('police trailer', \Snippet\LicensePlate::check('432 М 21'));
		$this->assertContains('police motorcycle', \Snippet\LicensePlate::check('0432 М 102'));

		$this->assertContains('ambassador', \Snippet\LicensePlate::check('091 CD 1 77'));
		$this->assertContains('diplomatic', \Snippet\LicensePlate::check('092 D 2 77'));

		$this->assertContains('diplomatic staff', \Snippet\LicensePlate::check('090T297'));

		$this->assertNotContains('motorcycle', \Snippet\LicensePlate::check('C091РС777'));

		$this->assertTrue(!\Snippet\LicensePlate::check('Я091РС777'));
	}
}