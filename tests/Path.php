<?php
/**
 * Created by PhpStorm.
 * User: avnov
 * Date: 10.01.2019
 * Time: 8:12
 */
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class Path extends TestCase
{
	public function testCheck()
	{
		require_once __DIR__ . "/../src/Snippet/Path.php";

		$path = new \Snippet\Path('/usr', 'local');
		$expected = DIRECTORY_SEPARATOR . 'usr' . DIRECTORY_SEPARATOR . 'local';

		$this->assertEquals($expected, $path->getPath(), 'path');
		$this->assertEquals($expected, \Snippet\Path::concatenation('/usr', 'local'), 'path static');

		$expected .= DIRECTORY_SEPARATOR . 'www';
		$path->add('www');

		$this->assertEquals($expected, $path->getPath(), 'path after add');

		$expected .= DIRECTORY_SEPARATOR;
		$path->add('/');
		$this->assertEquals($expected, $path->getPath(), 'path after add /');

		$path = new \Snippet\Path('/usr');
		$path
			->add('/local/')
			->add('/www/');
		$this->assertEquals($expected, $path->getPath(), 'path with duplicate "/"');
	}
}