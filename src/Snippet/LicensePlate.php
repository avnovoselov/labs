<?php

namespace Snippet;

use \Exception;

class LicensePlate
{
	/**
	 * license plate types by locale
	 */
	const LICENSE_PLATE_TYPE = [
		'RU' => [
			'private'           => 'Частный автомобиль',
			'legal'             => 'Автомобиль юр. лица',
			'transit'           => 'Транзитный',
			'bus'               => 'Муниципальный (Общественный) транспорт',
			'taxi'              => 'Такси',
			'trailer'           => 'Прицеп',
			'motorcycle'        => 'Мотоцикл',
			'motorbike'         => 'Мопед',
			'tractor'           => 'Сельскохозяйственная техника',
			'military'          => 'Военный автомобиль',
			'police'            => 'Полицейский автомобиль',
			'police trailer'    => 'Полицейский прицеп',
			'police motorcycle' => 'Полицейский мотоцикл',
			'ambassador'        => 'Посол или иное лицо в ранге главы дипломатического представительства',
			'diplomatic'        => 'Дипломатическое представительство',
			'diplomatic staff'  => 'Дипломатическое представительство, не обладающее дипломатическим статусом',
		],
	];

	/**
	 * license plate patterns by locale
	 */
	const LICENSE_PLATE_PATTERN = [
		'RU' => [
			'/^[АВЕКМНОРСТУХABEKMHOPCTYX]\d{3}[АВЕКМНОРСТУХABEKMHOPCTYX]{2}\d{2,3}$/u' => ['private', 'legal', 'transit'],
			'/^[АВЕКМНОРСТУХABEKMHOPCTYX]{2}\d{3}\d{2,3}$/u'                           => ['bus', 'taxi'],
			'/^[АВЕКМНОРСТУХABEKMHOPCTYX]{2}\d{4}\d{2,3}$/u'                           => ['trailer'],
			'/^\d{3}[CС]D\d{1,3}\d{2,3}$/u'                                            => ['ambassador'],
			'/^\d{3}D\d{1,3}\d{2,3}$/u'                                                => ['diplomatic'],
			'/^\d{3}[TТ]\d{1,3}\d{2,3}$/u'                                             => ['diplomatic staff'], // looks like police trailer
			'/^\d{4}[АВЕКМНОРСТУХABEKMHOPCTYX]{2}\d{2,3}$/u'                           => ['motorcycle', 'motorbike', 'tractor', 'military'],
			'/^[АВЕКМНОРСТУХABEKMHOPCTYX]\d{4}\d{2,3}$/u'                              => ['police'],
			'/^\d{3}[АВЕКМНОРСТУХABEKMHOPCTYX]\d{2,3}$/u'                              => ['police trailer'],
			'/^\d{4}[АВЕКМНОРСТУХABEKMHOPCTYX]\d{2,3}$/u'                              => ['police motorcycle'],

		],
	];

	/**
	 * license plate remove symbols patterns
	 */
	const LICENSE_PLATE_PREPARE_PATTERN = [
		'RU' => '/[^АВЕКМНОРСТУХABEKMHOPCTYXD\d]/u',
	];

	/**
	 * environment variable for license plate locale
	 *
	 * @var string
	 */
	public static $HASH_LICENSE_PLATE_LOCALE_ENVIRONMENT_KEY = 'LICENSE_PLATE_LOCALE';

	/**
	 * current locale
	 *
	 * @var string
	 */
	public static $LOCALE = 'RU';

	/**
	 * $licensePlate type detector
	 *
	 * @param string $licensePlate
	 * @param string $locale
	 * @return mixed
	 * @throws Exception
	 */
	public static function check(string $licensePlate, string $locale = '')
	{
		$environment_locale = getenv(static::$HASH_LICENSE_PLATE_LOCALE_ENVIRONMENT_KEY);

		if (!$locale && $environment_locale) {
			$locale = $environment_locale;
		} else if (!$locale) {
			$locale = static::$LOCALE;
		}

		$mask = static::LICENSE_PLATE_PATTERN[$locale] ?? null;

		if ($mask) {
			$licensePlate = mb_strtoupper($licensePlate, 'UTF-8');
			$licensePlate = preg_replace(static::LICENSE_PLATE_PREPARE_PATTERN[$locale], '', $licensePlate);

			foreach ($mask as $pattern => $type) {
				if (preg_match($pattern, $licensePlate)) {
					return $type;
				}
			}

			return [];
		} else {
			throw new Exception('Locale not found');
		}
	}
}