<?php
/**
 * Created by PhpStorm.
 * User: avnov
 * Date: 09.01.2019
 * Time: 19:10
 */

namespace Snippet;

/**
 * Class Path
 * @package Snippet
 */
class Path
{
	/**
	 * @var string
	 */
	protected $path = '';

	/**
	 * @param string ...$arguments
	 * @return string
	 */
	public static function concatenation(string ...$arguments)
	{
		foreach ($arguments as $key => $val) {
			if (!$key) {
				$arguments[$key] = rtrim(str_replace(['\\', '/'], DIRECTORY_SEPARATOR, $val), DIRECTORY_SEPARATOR);
			} elseif ($key == count($arguments) - 1) {
				$arguments[$key] = ltrim(str_replace(['\\', '/'], DIRECTORY_SEPARATOR, $val), DIRECTORY_SEPARATOR);
			} else {
				$arguments[$key] = trim(str_replace(['\\', '/'], DIRECTORY_SEPARATOR, $val), DIRECTORY_SEPARATOR);
			}
		}

		return implode(DIRECTORY_SEPARATOR, $arguments);
	}

	/**
	 * Path constructor.
	 * @param string ...$arguments
	 */
	public function __construct(string ...$arguments)
	{
		$this->path = call_user_func_array(['static', 'concatenation'], $arguments);

		return $this;
	}

	/**
	 * @param string ...$parts
	 * @return $this
	 */
	public function add(string ... $parts)
	{
		array_unshift($parts, $this->path);
		$this->path = call_user_func_array(['static', 'concatenation'], $parts);

		return $this;
	}

	public function getPath()
	{
		return (string)$this;
	}

	/**
	 * @return String
	 */
	public function __toString(): string
	{
		return $this->path;
	}
}