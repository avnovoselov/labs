# Labs
PHP Snippet-ы для ежедневного использования в PHP-проектах.
По-возможности стараюсь добавлять автотесты.

## Установка
```Shell
/> composer require anatoliy-novoselov/labs
```

---

## Password

Класс для работы с паролями

### Константы
- `CHECK_UPPER_CASE` - обязательное наличие символов в верхнем регистре
- `CHECK_LOWER_CASE` - обязательное наличие символов в нижнем регистре
- `CHECK_DIGIT` - обязательное наличие цифр
- `CHECK_SPEC` - обязательное наличие знаков препинания
- `CHECK_SPACE` - обязательное наличие пробела
- `CHECK_EASY` - обязательное наличие `CHECK_LOWER_CASE` и `CHECK_DIGIT`
- `CHECK_MEDIUM` - обязательное наличие `CHECK_EASY` и `CHECK_UPPER_CASE`
- `CHECK_STRONG` - обязательное наличие `CHECK_MEDIUM` и `CHECK_SPEC`
- `CHECK_IMPOSSIBLE` - обязательное наличие `CHECK_STRONG` и `CHECK_SPACE`
- `CHECK_ANY` - наличие символов не регламентируется
- `HASH_PASSWORD_SALT_ENVIRONMENT_KEY` - переменная среды, в которой хранится соль для паролей
- `COMPLEXITY_RETURN_AS_FLOAT` - получить значение сложности пароля как `float`
- `COMPLEXITY_NONE` - очень простой пароль (или входит в топ самых популярных)
- `COMPLEXITY_LOW` - простой пароль
- `COMPLEXITY_MEDIUM` - нормальный пароль
- `COMPLEXITY_STRONG` - хороший пароль
- `COMPLEXITY_IMPOSSIBLE` - отличный (сложный) пароль

### Переменные
- `$HASH_PASSWORD_SALT` - общая соль для паролей, еслозьуется если нет возможности хранить в переменных среды

### Методы
#### Проверка пароля
`check(string $password, int $rules, int $minLength = 6, float $maxLength = INF)` -
проверка пароля по определенным правилам и длине. Правила описаны в константах, начинающихся с префикса `CHECK_`.

```PHP
<?php
...
$validate = \Snippet\Password::check('fooBar', \Snippet\Password::CHECK_UPPER_CASE | \Snippet\Password::CHECK_LOWER_CASE, 3, 6);
var_dump($validate);
```

```Shell
/> bool(true)
```

#### Хеш пароля
`hash(string $password, string $dispersion = '', string $salt = '')` - создается `sha256` хэш от пароля с солью `$salt` и дисперсией `$dispersion`.

Дисперсия позволяет создавать хеши уникальными для разных пользователей с одинаковыми паролями. В качестве дисперсии лучше использовать дату регистрации пользователя или его идентификатор.

Лучше установить переменную среды `PASSWORD_HASH_SALT` или переменную из `\Snippet\Password::$HASH_PASSWORD_SALT_ENVIRONMENT_KEY`, она будет использована автоматически в качестве соли для пароля.

```PHP
<?php
echo \Snippet\Password::hash('foobar', '1', '**s<dsOsP?');
```

```Shell
/> dd79d7383ae053279bbd6481696c1d025543ad308e7f1567fa879625d851397e
```

#### Сложность пароля
`complexity(string $password, bool $as_float = false)` возвращает сложность пароля в виде `float` или `int` со степенью сложности.
`int` принимает одно из значений `COMPLEXITY_NONE`, `COMPLEXITY_LOW`, `COMPLEXITY_MEDIUM`, `COMPLEXITY_STRONG` или `COMPLEXITY_IMPOSSIBLE`

```PHP
<?php
var_dump(\Snippet\Password::complexity('foobar') == \Snippet\Password::COMPLEXITY_LOW);
```

```Shell
/> bool(true)
```
